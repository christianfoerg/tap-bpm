import { Component, HostListener } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent {
  title = 'tap-bpm';
  count = 0;
  stopped = true;
  firstPress: Date | null = null;
  lastPress: Date | null = null;
  timeout: number | null = null;

  isHit = false;
  hitTimeout: number | null = null;

  reset() {
    this.count = 0;
    this.firstPress = null;
    this.lastPress = null;
    this.stopped = true;
    if (this.timeout) {
      clearTimeout(this.timeout);
    }
  }

  @HostListener('document:keypress', ['$event'])
  handleKeyPress(event: KeyboardEvent) {
    if (event.key === ' ') {
      this.handleBeat();
    }
  }
  handleBeat() {
    if (this.stopped) {
      this.count = 0;
      this.firstPress = null;
      this.lastPress = null;
    }
    this.isHit = true;
    if (this.hitTimeout) {
      clearTimeout(this.hitTimeout);
    }
    this.hitTimeout = setTimeout(() => {
      this.isHit = false;
    }, 60);
    this.stopped = false;
    this.count++;
    if (this.firstPress === null) {
      this.firstPress = new Date();
    } else {
      this.lastPress = new Date();
    }
    if (this.timeout) clearTimeout(this.timeout);
    this.timeout = setTimeout(() => {
      this.stopped = true;
    }, 2000);
  }

  getBpm() {
    if (this.firstPress && this.lastPress) {
      return Math.round(
        (this.count - 1) /
          ((this.lastPress.valueOf() - this.firstPress.valueOf()) / (1000 * 60))
      );
    }
    return null;
  }
}
