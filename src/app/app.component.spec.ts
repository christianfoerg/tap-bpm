import { TestBed } from '@angular/core/testing';
import { AppComponent } from './app.component';

describe('AppComponent', () => {
  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [AppComponent],
    }).compileComponents();
  });

  it('should create the app', () => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.componentInstance;
    expect(app).toBeTruthy();
  });

  it(`should have as title 'tap-bpm'`, () => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.componentInstance;
    expect(app.title).toEqual('tap-bpm');
  });

  describe('getBpm', () => {
    it('should calculate bpm for 2 taps', () => {
      const fixture = TestBed.createComponent(AppComponent);
      const app = fixture.componentInstance;
      app.count = 2;
      app.firstPress = new Date('2020-12-30T12:30:00');
      app.lastPress = new Date('2020-12-30T12:31:00');
      expect(app.getBpm()).toBe(1);
    });
    it('should calculate bpm for 61 taps', () => {
      const fixture = TestBed.createComponent(AppComponent);
      const app = fixture.componentInstance;
      app.count = 121;
      app.firstPress = new Date('2020-12-30T12:30:00');
      app.lastPress = new Date('2020-12-30T12:31:00');
      expect(app.getBpm()).toBe(120);
    });
  });
});
